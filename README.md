# SEARCH GUIDE #

1. Modify the **def self.fuzzy_search** method in *app/models/product.rb*. Commit: fb5df94
2. Modify the search method in **app/controllers/products_controller.rb**. Commit: fbe40c3
2. Move the **_search.html.erb** file from *app/views/products* to *app/views*
3. De-comment the render line that has already been written at **app/views/layouts/application.html.erb**. Commit: b582f37
4. Add the *post route* to routes file at **config/routes.rb**. Commit: 7f3cd95