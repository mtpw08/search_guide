# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
MyBookshop2::Application.config.secret_key_base = 'a5eaf53f4e52166a37c1eaa5bfd1dc612a064639f93189843b0e2bfeb3f97b7ca3bdf9dac4bfab7c370d7a25ab39f9a63862eed3eb3dd33eeb6973ba7884e261'
